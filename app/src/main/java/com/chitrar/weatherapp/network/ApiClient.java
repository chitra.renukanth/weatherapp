package com.chitrar.weatherapp.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static Retrofit retrofit = null;
    private static String baseUrl = "https://api.openweathermap.org/data/2.5/";

    public static Retrofit getClient() {
        if(retrofit == null) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new QueryParameterAddInterceptor())
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}
