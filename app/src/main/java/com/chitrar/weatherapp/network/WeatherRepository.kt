package com.chitrar.weatherapp.network

import com.chitrar.weatherapp.model.WeatherInfoResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherRepository {
    var apiInterface: ApiInterface = ApiClient.getClient().create(ApiInterface::class.java)
    private lateinit var callback: ApiResponse<WeatherInfoResponse>

    fun getWeatherInfo(queryString: String, callback: ApiResponse<WeatherInfoResponse>){
        this.callback = callback
        val call: Call<WeatherInfoResponse> = apiInterface.getWeatherByQueryString(queryString)
        call.enqueue(responseCallback)
    }

    fun getWeatherByLatLon(latitude: Double, longitude: Double, callback: ApiResponse<WeatherInfoResponse>){
        this.callback = callback
        val call: Call<WeatherInfoResponse> = apiInterface.getWeatherDataByLatLon(latitude.toString(), longitude.toString())
        call.enqueue(responseCallback)
    }

    var responseCallback = object : Callback<WeatherInfoResponse> {
        // if retrofit network call success, this method will be triggered
        override fun onResponse(
            call: Call<WeatherInfoResponse>,
            response: Response<WeatherInfoResponse>
        ) {
            if (response.body() != null)
                callback.onSuccess(requireNotNull(response.body())) //let presenter know the weather information data
            else
                callback.onError() //let presenter know about failure
        }

        // this method will be triggered if network call failed
        override fun onFailure(call: Call<WeatherInfoResponse>, t: Throwable) {
            callback.onError()//let presenter know about failure
        }
    }

    interface ApiResponse<T> {
        fun onSuccess(data: T)
        fun onError()
    }
}