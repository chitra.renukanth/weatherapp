package com.chitrar.weatherapp.network

import android.graphics.ColorSpace.Model
import com.chitrar.weatherapp.model.WeatherInfoResponse
import retrofit2.Call

import retrofit2.http.GET
import retrofit2.http.Query


interface ApiInterface {
    @GET("weather")
    fun getWeatherByQueryString(@Query("q") cityId: String): Call<WeatherInfoResponse>

    @GET("weather")
    fun getWeatherDataByLatLon(
        @Query("lat") query_lat: String?,
        @Query("lon") query_lon: String?
    ): Call<WeatherInfoResponse>
}