package com.chitrar.weatherapp.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class QueryParameterAddInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        String url = chain.request().url().newBuilder()
                .addQueryParameter("appid", "0dcda9b4a40e745c91d752b6122a51bd")
                .build().toString();
        Request request = chain.request().newBuilder().url(url).build();
        return chain.proceed(request);
    }
}
