package com.chitrar.weatherapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chitrar.weatherapp.model.WeatherData
import com.chitrar.weatherapp.model.WeatherInfoResponse
import com.chitrar.weatherapp.network.WeatherRepository
import com.chitrar.weatherapp.utils.kelvinToCelsius
import com.chitrar.weatherapp.utils.unixTimestampToDateTimeString
import com.chitrar.weatherapp.utils.unixTimestampToTimeString

class WeatherMainViewModel: ViewModel() {

    val progressBarLiveData = MutableLiveData<Boolean>()
    val weatherInfoLiveData = MutableLiveData<WeatherData>()
    val weatherInfoFailureLiveData = MutableLiveData<String>()

    var apiResponse = object :
        WeatherRepository.ApiResponse<WeatherInfoResponse> {
        override fun onSuccess(data: WeatherInfoResponse) {

            // business logic and data manipulation tasks should be done here
            val weatherData = WeatherData(
                dateTime = data.dt.unixTimestampToDateTimeString(),
                temperature = data.main.temp.kelvinToCelsius().toString(),
                cityAndCountry = "${data.name}, ${data.sys.country}",
                weatherConditionIconUrl = "http://openweathermap.org/img/w/${data.weather[0].icon}.png",
                weatherConditionIconDescription = data.weather[0].description,
                humidity = "${data.main.humidity}%",
                pressure = "${data.main.pressure} mBar",
                visibility = "${data.visibility/1000.0} KM",
                sunrise = data.sys.sunrise.unixTimestampToTimeString(),
                sunset = data.sys.sunset.unixTimestampToTimeString()
            )

            progressBarLiveData.postValue(false) // PUSH data to LiveData object to hide progress bar

            // After applying business logic and data manipulation, we push data to show on UI
            weatherInfoLiveData.postValue(weatherData) // PUSH data to LiveData object
        }

        override fun onError() {
            progressBarLiveData.postValue(false) // hide progress bar
            weatherInfoFailureLiveData.postValue("No data found. Try again.") // PUSH error message to LiveData object
        }
    }

    fun getWeatherInfo(queryString: String) {

        progressBarLiveData.postValue(true) // PUSH data to LiveData object to show progress bar

        WeatherRepository().getWeatherInfo(queryString, apiResponse)
    }

    fun getWeatherByLatLon(latitude: Double, longitude: Double){

        progressBarLiveData.postValue(true) // PUSH data to LiveData object to show progress bar

        WeatherRepository().getWeatherByLatLon(latitude, longitude, apiResponse)
    }
}