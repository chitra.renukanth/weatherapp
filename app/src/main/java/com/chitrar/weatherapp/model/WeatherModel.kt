package com.chitrar.weatherapp.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class City(
    val id: Int = 0,
    val name: String = "",
    val country: String = ""
): Parcelable

@Parcelize
data class Clouds(
    val all: Int = 0
): Parcelable

@Parcelize
data class Coord(
    val lon: Double = 0.0,
    val lat: Double = 0.0
): Parcelable

@Parcelize
data class Main(
    val temp: Double = 0.0,
    val pressure: Double = 0.0,
    val humidity: Int = 0,
    @SerializedName("temp_min")
    val tempMin: Double = 0.0,
    @SerializedName("temp_max")
    val tempMax: Double = 0.0
): Parcelable

@Parcelize
data class Sys(
    val type: Int = 0,
    val id: Int = 0,
    val message: Double = 0.0,
    val country: String = "",
    val sunrise: Int = 0,
    val sunset: Int = 0
): Parcelable

@Parcelize
data class Weather(
    val id: Int = 0,
    val main: String = "",
    val description: String = "",
    val icon: String = ""
): Parcelable

@Parcelize
data class WeatherData(
    var dateTime: String = "",
    var temperature: String = "0",
    var cityAndCountry: String = "",
    var weatherConditionIconUrl: String = "",
    var weatherConditionIconDescription: String = "",
    var humidity: String = "",
    var pressure: String = "",
    var visibility: String = "",
    var sunrise: String = "",
    var sunset: String = ""
): Parcelable

@Parcelize
data class WeatherInfoResponse(
    val coord: Coord = Coord(),
    val weather: List<Weather> = listOf(),
    val base: String = "",
    val main: Main = Main(),
    val visibility: Int = 0,
    val wind: Wind = Wind(),
    val clouds: Clouds = Clouds(),
    val dt: Int = 0,
    val sys: Sys = Sys(),
    val id: Int = 0,
    val name: String = "",
    val cod: Int = 0
): Parcelable

@Parcelize
data class Wind(
    val speed: Double = 0.0,
    val deg: Double = 0.0
): Parcelable