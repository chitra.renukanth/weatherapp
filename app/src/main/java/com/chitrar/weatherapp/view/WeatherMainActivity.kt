package com.chitrar.weatherapp.view

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.chitrar.weatherapp.R
import com.chitrar.weatherapp.databinding.ActivityMainBinding
import com.chitrar.weatherapp.model.WeatherData
import com.chitrar.weatherapp.viewmodel.WeatherMainViewModel
import com.google.android.gms.location.*


class WeatherMainActivity : AppCompatActivity() {

    private lateinit var viewModel: WeatherMainViewModel
    private lateinit var binding: ActivityMainBinding
    private lateinit var sharedPref: SharedPreferences
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.let {
            title = getString(R.string.weather_title)
        }
        // initialize ViewModel
        viewModel = ViewModelProvider(this)[WeatherMainViewModel::class.java]
        sharedPref = this.getSharedPreferences("my-pref", Context.MODE_PRIVATE)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        // set LiveData and View click listeners before the call for data fetching
        setLiveDataListeners()
        setSearchClickListener()
        loadData()
    }

    private fun loadData() {
        if(!getLocationIfPermission()) {
            val lastQuery = sharedPref.getString("lastQuery", null)
            if (lastQuery != null) {
                viewModel.getWeatherInfo(lastQuery)
            } else {
                requestLocationPermission()
            }
        }
    }

    private fun getLocationIfPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    if(location != null) {
                        viewModel.getWeatherByLatLon(location.latitude, location.longitude)
                    }

                }
            return true
        }
        return false
    }

    private fun setSearchClickListener() {
        binding.apply {
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    if (!query.isNullOrEmpty()) {
                        viewModel.getWeatherInfo(query)
                        with(sharedPref.edit()) {
                            putString("lastQuery", query)
                            apply()
                        }
                    }
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                   return false
                }
            })
            searchByLocation.setOnClickListener{
                if(!getLocationIfPermission()){
                    requestLocationPermission()
                }
            }
        }
    }

    private fun setLiveDataListeners() {
        /**
         * ProgressBar visibility will be handled by this LiveData. ViewModel decides when Activity
         * should show ProgressBar and when hide.
         *
         * Here I've used lambda expression to implement Observer interface in second parameter.
         */
        viewModel.progressBarLiveData.observe(this, Observer { isShowLoader ->
            if (isShowLoader)
                binding.progressBar.visibility = View.VISIBLE
            else
                binding.progressBar.visibility = View.GONE
        })

        /**
         * This method will be triggered when ViewModel successfully receive WeatherData from our
         * data source (I mean Model). Activity just observing (subscribing) this LiveData for showing
         * weather information on UI. ViewModel receives Weather data API response from Model via
         * Callback method of Model. Then ViewModel apply some business logic and manipulate data.
         * Finally ViewModel PUSH WeatherData to `weatherInfoLiveData`. After PUSHING into it, below
         * method triggered instantly! Then we set the data on UI.
         *
         * Here I've used lambda expression to implement Observer interface in second parameter.
         */
        viewModel.weatherInfoLiveData.observe(this, Observer { weatherData ->
            setWeatherInfo(weatherData)
        })
        viewModel.weatherInfoFailureLiveData.observe(this, Observer {
            binding.apply {
                outputGroup.visibility = View.GONE
                tvErrorMessage.visibility = View.VISIBLE
                tvErrorMessage.text = getString(R.string.error_message)
            }
            Toast.makeText(this, getString(R.string.error_message), Toast.LENGTH_LONG).show()
        })

    }

    private fun requestLocationPermission() {
        val locationPermissionRequest = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { permissions ->
            when {
                permissions.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                    val locationRequest = LocationRequest()
                    locationRequest.interval = 10000
                    locationRequest.fastestInterval = 3000
                    locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

                    if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return@registerForActivityResult
                    }
                    fusedLocationClient.requestLocationUpdates(locationRequest, object:
                        LocationCallback() {


                        override fun onLocationResult(locationResult: LocationResult) {
                            super.onLocationResult(locationResult)
                            LocationServices.getFusedLocationProviderClient(getApplicationContext())
                                .removeLocationUpdates(this)
                            if (locationResult.locations.size > 0) {
                                val latestLocIndex = locationResult.locations.size - 1;
                                val latitude = locationResult.locations[latestLocIndex].latitude;
                                val longitude = locationResult.locations[latestLocIndex].longitude;
                                viewModel.getWeatherByLatLon(latitude, longitude)
                            }
                        }
                    }, Looper.getMainLooper())
                }
                else -> {
                    Toast.makeText(this, getString(R.string.tunr_on_location), Toast.LENGTH_LONG).show()
                }
            }
        }

        locationPermissionRequest.launch(
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )
    }

    private fun setWeatherInfo(weatherData: WeatherData) {
        binding.apply {
            outputGroup.visibility = View.VISIBLE
            tvErrorMessage.visibility = View.GONE

            layoutWeatherBasic.apply {
                tvDateTime.text = weatherData.dateTime
                tvTemperature.text = weatherData.temperature
                tvCityCountry.text = weatherData.cityAndCountry
                Glide.with(this@WeatherMainActivity).load(weatherData.weatherConditionIconUrl)
                    .into(ivWeatherCondition)
                tvWeatherCondition.text = weatherData.weatherConditionIconDescription
            }

            layoutWeatherAdditional.apply {
                tvHumidityValue.text = weatherData.humidity
                tvPressureValue.text = weatherData.pressure
                tvVisibilityValue.text = weatherData.visibility
            }

            layoutSunsetSunrise.apply {
                tvSunriseTime.text = weatherData.sunrise
                tvSunsetTime.text = weatherData.sunset
            }

        }
    }

}